import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Button } from 'semantic-ui-react';
import * as postService from 'src/services/postService';

import styles from './styles.module.scss';

const DeletePost = ({ id, close }) => {
  const [deleted, setDeleted] = useState(undefined);
  const handleDeletePost = async () => {
    const response = await postService.deletePost(id);
    if (response) {
      setDeleted(true);
      close(id);
    } else {
      setDeleted(false);
    }
  };

  return (
    <Modal size="mini" open onClose={() => close(id)}>
      <Modal.Header className={styles.header}>
        <span>Delete Post?</span>
        {deleted === false && (
          <span>
            <Icon color="red" name="check" />
            Something went wrong :\
          </span>
        )}
      </Modal.Header>
      <Modal.Content style={{ display: 'flex' }}>
        <Button
          style={{ float: 'none', margin: 'auto' }}
          floated="right"
          color="red"
          onClick={handleDeletePost}
        >
          Delete post
        </Button>
      </Modal.Content>
    </Modal>
  );
};

DeletePost.propTypes = {
  id: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default DeletePost;
