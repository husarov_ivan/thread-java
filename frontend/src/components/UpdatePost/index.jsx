import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Form, Image, Button } from 'semantic-ui-react';
import * as postService from 'src/services/postService';
import * as imageService from 'src/services/imageService';

import styles from './styles.module.scss';

const UpdatePost = ({ postBody, close }) => {
  const [updated, setUpdated] = useState(false);
  const [body, setBody] = useState(postBody.body);
  const [image, setImage] = useState(postBody.image);
  const [isUploading, setIsUploading] = useState(false);
  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    // eslint-disable-next-line no-unused-vars
    const response = await postService.updatePost({ postId: postBody.id, body, imageId: image?.imageId });
    setUpdated(true);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await imageService.uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal open onClose={() => close(postBody.id)} size="small">
      <Modal.Header className={styles.header}>
        <span>Update Post</span>
        {updated && (
          <span>
            <Icon color="green" name="check" />
            Post updated!
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleAddPost}>
          <Form.TextArea
            name="body"
            defaultValue={postBody.body}
            onChange={ev => setBody(ev.target.value)}
          />
          {image?.imageLink && (
            <div className={styles.imageWrapper}>
              <Image className={styles.image} src={image?.imageLink} alt="post" />
            </div>
          )}
          <Button
            color="teal"
            icon
            labelPosition="left"
            as="label"
            loading={isUploading}
            disabled={isUploading}
          >
            <Icon name="image" />
            Attach image
            <input name="image" type="file" onChange={handleUploadFile} hidden />
          </Button>
          <Button floated="right" color="red" type="button" onClick={() => setImage(null)}>Remove Image</Button>
          <Button floated="right" color="blue" type="submit">Update post</Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  postBody: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatePost;
