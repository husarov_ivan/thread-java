import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Button } from 'semantic-ui-react';
import { deleteComment } from 'src/services/commentService';

import styles from './styles.module.scss';

const DeleteComment = ({ id, close }) => {
  const [deleted, setDeleted] = useState(undefined);
  const handleDeleteComment = async () => {
    const response = await deleteComment(id);
    if (response) {
      setDeleted(true);
      close(id);
    } else {
      setDeleted(false);
    }
  };

  return (
    <Modal size="mini" open onClose={() => close(id)}>
      <Modal.Header className={styles.header}>
        <span>Delete Comment?</span>
        {deleted === false && (
          <span>
            <Icon color="red" name="check" />
            Something went wrong :\
          </span>
        )}
      </Modal.Header>
      <Modal.Content style={{ display: 'flex' }}>
        <Button
          style={{ float: 'none', margin: 'auto' }}
          floated="right"
          color="red"
          onClick={handleDeleteComment}
        >
          Delete comment
        </Button>
      </Modal.Content>
    </Modal>
  );
};

DeleteComment.propTypes = {
  id: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default DeleteComment;
