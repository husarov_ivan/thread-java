import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Form, Button } from 'semantic-ui-react';
import { updateComment } from 'src/services/commentService';
import styles from './styles.module.scss';

const UpdateComment = ({ commentBody, close }) => {
  const [updated, setUpdated] = useState(false);
  const [body, setBody] = useState(commentBody.body);
  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    // eslint-disable-next-line no-unused-vars
    const response = await updateComment({ id: commentBody.id, body });
    setUpdated(true);
  };

  return (
    <Modal open onClose={() => close()} size="small">
      <Modal.Header className={styles.header}>
        <span>Update Comment</span>
        {updated && (
          <span>
            <Icon color="green" name="check" />
            Comment updated!
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUpdateComment}>
          <Form.TextArea
            name="body"
            defaultValue={commentBody.body}
            onChange={ev => setBody(ev.target.value)}
          />
          <Button color="blue" type="submit">Update comment</Button>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdateComment.propTypes = {
  commentBody: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired
};

export default UpdateComment;
