import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment: { id, body, createdAt, user, selfComment }, updateComment, deleteComment }) => (
  <CommentUI className={styles.comment}>
    <CommentUI.Avatar src={getUserImgLink(user.image)} />
    <CommentUI.Content>
      <CommentUI.Author as="a">
        {user.username}
      </CommentUI.Author>
      <CommentUI.Metadata>
        {moment(createdAt).fromNow()}
      </CommentUI.Metadata>
      <CommentUI.Text>
        {body}
      </CommentUI.Text>
      <CommentUI.Actions>
        {selfComment && (
          <CommentUI.Action>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => deleteComment(id)}
            >
              <Icon name="trash alternate" />
            </Label>
          </CommentUI.Action>
        )}
        {selfComment && (
          <CommentUI.Action>
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={() => updateComment(id, body)}
            >
              <Icon name="pencil alternate" />
            </Label>
          </CommentUI.Action>
        )}

      </CommentUI.Actions>
    </CommentUI.Content>
  </CommentUI>
);

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  selfComment: PropTypes.bool.isRequired
};

export default Comment;
