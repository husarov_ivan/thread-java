import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { likePost, toggleExpandedPost, addComment } from 'src/containers/Thread/actions';
import { getPost } from 'src/services/postService';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import UpdateComment from '../../components/UpdateComment';
import DeleteComment from '../../components/DeleteComment';

const ExpandedPost = ({
  post,
  sharePost,
  updatePost,
  deletePost,
  likePost: like,
  likePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add
}) => {
  const [updatedComment, setUpdatedComment] = useState(undefined);
  const [deletedComment, setDeletedComment] = useState(undefined);
  const [reloaded, setReloaded] = useState(post);

  const updateComment = (id, body) => {
    setUpdatedComment({ id, body });
  };

  const deleteComment = id => {
    setDeletedComment(id);
  };

  const commentsLiveUpdate = async () => {
    await setReloaded(await getPost(reloaded.id));
    setUpdatedComment(undefined);
    setDeletedComment(undefined);
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              dislikePost={dislike}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              updatePost={updatePost}
              deletePost={deletePost}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {reloaded.comments && reloaded.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    updateComment={updateComment}
                    deleteComment={deleteComment}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} close={() => commentsLiveUpdate()} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
      {updatedComment && (
        <UpdateComment
          commentBody={updatedComment}
          close={() => commentsLiveUpdate()}
        />
      )}
      {deletedComment && (
        <DeleteComment
          id={deletedComment}
          close={() => commentsLiveUpdate()}
        />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = { likePost, toggleExpandedPost, addComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
