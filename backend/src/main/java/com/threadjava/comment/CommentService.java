package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.post.PostsRepository;
import com.threadjava.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private PostsRepository postsRepository;

    public CommentDetailsDto getCommentById(UUID id) {
        var comment = commentRepository.findById(id).orElseThrow();
        return !comment.getDeleted() ? CommentMapper.MAPPER.commentToCommentDetailsDto(comment) : null;
    }

    public CommentDetailsDto create(CommentSaveDto commentDto) {
        var comment = CommentMapper.MAPPER.commentSaveDtoToModel(commentDto);
        var postCreated = commentRepository.save(comment);
        return CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated);
    }

    public Optional<CommentDetailsDto> update(CommentUpdateDto commentUpdateDto) {
        var comment = commentRepository.findById(commentUpdateDto.getId()).orElseThrow();
        if (!comment.getUser().getId().equals(commentUpdateDto.getUserId()))
            return Optional.empty();
        comment.setBody(commentUpdateDto.getBody());
        var postCreated = commentRepository.save(comment);
        return Optional.of(CommentMapper.MAPPER.commentToCommentDetailsDto(postCreated));
    }

    public boolean delete(UUID commentId, UUID userId) {
        var comment = commentRepository.findById(commentId).orElseThrow();
        if (comment.getDeleted() || !comment.getUser().getId().equals(userId))
            return false;
        comment.setDeleted(true);
        commentRepository.save(comment);
        return true;
    }
}
