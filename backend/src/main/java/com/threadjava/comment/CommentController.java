package com.threadjava.comment;

import com.threadjava.comment.dto.CommentDetailsDto;
import com.threadjava.comment.dto.CommentSaveDto;
import com.threadjava.comment.dto.CommentUpdateDto;
import com.threadjava.post.dto.PostCreationResponseDto;
import com.threadjava.post.dto.PostUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;
import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/comments")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/{id}")
    public CommentDetailsDto get(@PathVariable UUID id) {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDetailsDto post(@RequestBody CommentSaveDto commentDto) {
        commentDto.setUserId(getUserId());
        return commentService.create(commentDto);
    }

    @PutMapping
    public Optional<CommentDetailsDto> put(@RequestBody CommentUpdateDto commentUpdateDto){
        commentUpdateDto.setUserId(getUserId());
        return commentService.update(commentUpdateDto);
    }

    @DeleteMapping
    public boolean delete(@RequestBody UUID commentId){
       return commentService.delete(commentId, getUserId());
    }
}
