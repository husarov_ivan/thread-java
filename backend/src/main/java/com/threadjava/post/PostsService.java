package com.threadjava.post;

import com.threadjava.comment.CommentRepository;
import com.threadjava.post.dto.*;
import com.threadjava.post.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.Optional;

@Service
public class PostsService {
    @Autowired
    private PostsRepository postsCrudRepository;
    @Autowired
    private CommentRepository commentRepository;

    public List<PostListDto> getAllPosts(Integer from, Integer count, UUID userId, UUID thisUser) {
        var pageable = PageRequest.of(from / count, count);
        var postList = postsCrudRepository
                .findAllPosts(userId, pageable)
                .stream()
                .map(PostMapper.MAPPER::postListToPostListDto)
                .filter(Predicate.not(PostListDto::isDeleted))
                .peek(x -> {
                    if (x.getUser().getId().equals(thisUser)) {
                        x.setSelfPost(true);
                    } else {
                        x.setSelfPost(false);
                    }
                })
                .collect(Collectors.toList());
        return postList;
    }

    public PostDetailsDto getPostById(UUID id, UUID thisUser) {
        var post = postsCrudRepository.findPostById(id)
                .map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow();
        if (post.isDeleted()) {
            throw new NoSuchElementException("Element has been deleted");
        }
        if (post.getUser().getId().equals(thisUser)) {
            post.setSelfPost(true);
        } else {
            post.setSelfPost(false);
        }
        var comments = commentRepository.findAllByPostId(id)
                .stream()
                .map(PostMapper.MAPPER::commentToCommentDto)
                .filter(Predicate.not(PostCommentDto::isDeleted))
                .peek(x -> {
                    if (x.getUser().getId().equals(thisUser)) {
                        x.setSelfComment(true);
                    } else {
                        x.setSelfComment(false);
                    }
                }).collect(Collectors.toList());
        post.setComments(comments);

        return post;
    }

    public PostCreationResponseDto create(PostCreationDto postDto) {
        Post post = PostMapper.MAPPER.postCreationDtoToPost(postDto);
        Post postCreated = postsCrudRepository.save(post);
        return PostMapper.MAPPER.postToPostCreationResponseDto(postCreated);
    }

    public Optional<PostCreationResponseDto> update(PostUpdateDto postDto) {
        if (!postsCrudRepository.findPostById(postDto.getPostId()).map(PostMapper.MAPPER::postToPostDetailsDto)
                .orElseThrow().getUser().getId().equals(postDto.getUserId()))
            return Optional.empty();
        Post post = PostMapper.MAPPER.postUpdateDtoToPost(postDto);
        Post postUpdated = postsCrudRepository.save(post);
        return Optional.of(PostMapper.MAPPER.postToPostCreationResponseDto(postUpdated));
    }

    public boolean delete(UUID postId, UUID thisUser) {
        try {
            var post = getPostById(postId, thisUser);
            post.setDeleted(true);
            postsCrudRepository.save(PostMapper.MAPPER.postDetailsDtoToPost(post));
            return true;
        } catch (NoSuchElementException ex) {
            return false;
        }


    }
}
