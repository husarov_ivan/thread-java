package com.threadjava.post.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PostUpdateDto {
    private UUID userId;
    private UUID postId;
    private String body;
    private UUID imageId;
}
